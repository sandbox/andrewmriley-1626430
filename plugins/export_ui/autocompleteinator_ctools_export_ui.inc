<?php

/**
 * Define this Export UI plugin.
 */
$plugin = array(
  'schema' => 'autocompleteinator',
  'access' => 'administer autocompleteinator',

  // Define the menu item.
  'menu' => array(
    'menu item' => 'autocompleteinator',
    'menu title' => 'Autocompleteinator',
    'menu description' => 'Behold! Configure the autocompleteinator!',
  ),

  // Define user interface texts.
  'title singular' => t('record'),
  'title plural' => t('records'),
  'title singular proper' => t('Autocompleteinator record'),
  'title plural proper' => t('Autocompleteinator records'),

  // Define the names of the functions that provide the add/edit forms.
  'form' => array(
    'settings' => 'autocompleteinator_ctools_export_ui_form',
    // 'submit' and 'validate' are also valid callbacks.
  ),

  'handler' => array(
    'class' => 'ctools_export_ui_autocompleteinator',
    'parent' => 'ctools_export_ui',
  ),
);

/**
 * Defines the record add/edit form.
 */
function autocompleteinator_ctools_export_ui_form(&$form, &$form_state) {
  $record = $form_state['item'];

  // Support for Autocompleteinator Helper module.
  if (arg(4) != NULL) {
    $record->target_form_id = check_plain(arg(4));
  }
  if (arg(5) != NULL) {
    $record->field_id = check_plain(arg(5));
  }

  $form['name'] = array(
    '#type' => 'textfield',
    '#size' => '50',
    '#maxlength' => 64,
    '#title' => t('Name'),
    '#default_value' => $record->name,
    '#description' => t('The user friendly name that will list in the administration section.'),
    '#required' => TRUE,
  );

  $form['target_form_id'] = array(
    '#type' => 'textfield',
    '#size' => '50',
    '#title' => t('Target Form ID'),
    '#default_value' => $record->target_form_id,
    '#description' => t('The Form ID of the form where the field appears.'),
    '#element_validate' => array('autocompleteinator_id_validate'),
    '#required' => TRUE,
  );

  $form['field_id'] = array(
    '#type' => 'textfield',
    '#size' => '50',
    '#title' => t('Field ID'),
    '#default_value' => $record->field_id,
    '#description' => t('The Field ID of field to be autocompleteinatored.'),
    '#element_validate' => array('autocompleteinator_id_validate'),
    '#required' => TRUE,
  );

  $form['view_path'] = array(
    '#type' => 'textfield',
    '#size' => '50',
    '#title' => t('View path'),
    '#default_value' => $record->view_path,
    '#description' => t('The path to the view that will provide the autocompleteinator options.'),
    '#element_validate' => array('autocompleteinator_path_validate'),
    '#required' => TRUE,
  );
}

/**
 * Element validate function to help ensure the form and field IDs are proper.
 */
function autocompleteinator_id_validate($element, $form_state) {
  if (preg_match('/[^0-9a-z_\-]/', $element['#value'])) {
    form_set_error($element['#name'], t("This doesn't look correct. Please only use lowercase alphanumeric characters, underscores (_), and hyphens (-) for Form and Field IDs."));
  }
}

/**
 * Element validate function to help ensure the view paths are proper.
 */
function autocompleteinator_path_validate($element, $form_state) {
  if (preg_match('/[^0-9a-z_\-\/]/', $element['#value'])) {
    form_set_error($element['#name'], t("This doesn't look correct. Please only use lowercase alphanumeric characters, underscores (_), hyphens (-), and forward-slashes (/) for view paths."));
  }
}
