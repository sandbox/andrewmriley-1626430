<?php

class ctools_export_ui_autocompleteinator extends ctools_export_ui {
  /**
   * Adds Autocompletinator data cells to the UI table.
   */
  function list_build_row($item, &$form_state, $operations) {
    parent::list_build_row($item, $form_state, $operations);
    $name = $item->{$this->plugin['export']['key']};
    $additional_cells = array(
      array(
        'data' => $item->name,
        'class' => array('ctools-export-ui-human-name'),
      ),
      array(
        'data' => $item->target_form_id,
        'class' => array('ctools-export-ui-target-form-id'),
      ),
      array(
        'data' => $item->field_id,
        'class' => array('ctools-export-ui-field-id'),
      ),
    );
    array_splice($this->rows[$name]['data'], 1, 0, $additional_cells);
  }

  /**
   * Adds Autocompletinator data header cells to the UI table.
   */
  function list_table_header() {
    $header = parent::list_table_header();
    $header[0]['data'] = t('Machine Name');
    $additional_headers = array(
      array(
        'data' => t('Name'),
        'class' => array('ctools-export-ui-human-name'),
      ),
      array(
        'data' => t('Target Form ID'),
        'class' => array('ctools-export-ui-target-form-id'),
      ),
      array(
        'data' => t('Field ID'),
        'class' => array('ctools-export-ui-field-id'),
      ),
    );
    array_splice($header, 1, 0, $additional_headers);
    return $header;
  }
}
