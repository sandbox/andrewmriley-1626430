<?php

/**
 * @file
 * Theme functions for Views RSS module.
 */

/**
 * Template preprocessor for views-view-views-rss.tpl.php.
 */
function template_preprocess_views_view_autocompleteinator(&$vars) {
  $view = $vars['view'];
  $fields = $view->field;
  $view_rows = $vars['rows'];
  $options = $vars['options'];
  $rows = array();
  foreach ($view_rows as $row) {
    foreach ($fields as $name => $field) {
      //dsm($view->field['field_company']->advanced_render($row));
      $temp = $view->field[$name]->advanced_render($row);
      if ($name == $options['key']) {
        $new_key = $temp;
      }
      elseif ($name == $options['value']) {
        $new_value = $temp;
      }
    }
    $rows[$new_key] = $new_value;
  }
  if (empty($vars['view']->live_preview)) {
    drupal_add_http_header('Content-Type', 'application/json');
    echo drupal_json_encode($rows);
    exit();
  }
}
