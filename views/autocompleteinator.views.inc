<?php

/*
 * Implements hook_views_plugins()
 */
function autocompleteinator_views_plugins() {
  return array(
    'style' => array(
      'autocompleteinator' => array(
        'type' => 'feed',
        'title' => t('AutocompleteInator'),
        'help' => t('Allows Views to output data in an autocomplete friendy format.'),
        'handler' => 'autocompleteinator_plugin_style_fields',
        'path' => drupal_get_path('module', 'autocompleteinator') . '/views',
        'theme' => 'views_view_autocompleteinator',
        'theme file' => 'theme.inc',
        'theme path' => drupal_get_path('module', 'autocompleteinator') . '/theme',
        'even empty' => TRUE,
        'uses row plugin' => FALSE,
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'uses grouping' => FALSE,
      ),
    ),
  );
}

/*
 * Implements hook_views_plugins()
 */
/*function autocompleteinator_views_query_alter(&$view, &$query) {
  // This boolean value is set in the init of class autocompleteinator_plugin_style_fields
  // it is a temporary workaround
  if (isset($view->autocompleteinator)) {
    $conditions = &$query->where[0]['conditions'];
    foreach ($conditions as &$condition) {
      $condition['value'] = '%' . $condition['value'] . '%';
      $condition['operator'] = 'LIKE';
    }
  }
}*/
