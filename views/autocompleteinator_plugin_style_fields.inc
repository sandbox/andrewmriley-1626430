<?php

/**
 * @file
 * Extends the view_plugin_style class to provide new RSS view style.
 */
class autocompleteinator_plugin_style_fields extends views_plugin_style {

  function init(&$view, &$options) {
    parent::init($view, $options);
    // This is a lame work around until I can find an easy way to determine
    // which style is being used
    $view->autocompleteinator = TRUE;
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['key'] = array('default' => '');
    $options['value'] = array('default' => '');

    return $options;
  }

  /**
   * Provide a form for setting options.
   *
   * @param array $form
   * @param array $form_state
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $fields = array('' => t('<None>'));
    foreach ($this->display->handler->get_handlers('field') as $field => $handler) {
      if ($label = $handler->label()) {
        $fields[$field] = $label;
      }
      else {
        $fields[$field] = $handler->ui_name();
      }
    }

    $form['key'] = array(
      '#type' => 'select',
      '#title' => t('Key'),
      '#options' => $fields,
      '#default_value' => $this->options['key'],
      '#description' => t('Select the field that will be the unique identifier for earch row.'),
      '#required' => TRUE,
    );
    $form['value'] = array(
      '#type' => 'select',
      '#title' => t('Value'),
      '#options' => $fields,
      '#default_value' => $this->options['value'],
      '#description' => t('Select the field that will be displayed to the user in the autocomplete.'),
      '#required' => TRUE,
    );
  }
}
